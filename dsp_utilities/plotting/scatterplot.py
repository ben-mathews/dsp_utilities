import numpy as np
import matplotlib.pyplot as plt

def scatterplot(iq, labels=[], fig=None):
    """
    Generate a scatter plot for a set of IQ data provided either as an numpy.ndarray or a list of numpy.ndarray's
    :param iq: IQ data to plot (typically constellations)
    :param labels: Labels for plot.  If iq is a single numpy.ndarray, then this should be a string.  If iq us a list of numpy.ndarray's then this should be a list of strings
    :return: Figure object
    """
    # Do some basic type-checking
    assert ((type(iq) is list) and (len(set(([type(e) for e in iq])))==1) and (list(set(([type(e) for e in iq])))[0] is np.ndarray)) or type(iq) is np.ndarray, "iq must be of type numpy.ndarray or list of numpy.ndarray arrays"
    if not (type(labels) is list and not labels): # Don't check labels if it is an empty list
        if type(iq) is list:
            assert ((type(labels) is list) and (len(set(([type(e) for e in labels])))==1) and (list(set(([type(e) for e in labels])))[0] is str)), "If type(iq) is list, then labels must be a list of strings"
        elif type(iq) is np.ndarray:
            assert type(labels) is str, "If type(iq) is numpy.ndarray, then labels must be a string or a list of strings"
        else:
            raise AssertionError('If type(iq) is list, then labels must be a list of strings.  If type(iq) is numpy.ndarray, then labels must be a string or a list of strings.  Neither of these conditions have been met')

    if type(iq) is np.ndarray:
        iq = [iq]
        if type(labels) is str:
            labels = [labels]

    if fig is None:
        fig = plt.figure()

    iq_dims = np.shape(iq)
    max_abs_val = 0.0

    for idx, iq_ in enumerate(iq):
        if type(labels) is list and len(labels) == 0:
            label = 'Trace {}'.format(idx)
        else:
            label = labels[idx]
        if np.max(np.abs(iq_)) > max_abs_val:
            max_abs_val = np.max(np.abs(iq_))

        plt.plot(np.real(iq_), np.imag(iq_), '.', label=label)

    limits = 1.1 * max_abs_val;
    plt.xlim(-limits, limits)
    plt.ylim(-limits, limits)
    plt.grid(True)
    plt.ylabel('Quadrature');
    plt.xlabel('In-Phase');
    plt.title('Scatter plot');
    plt.axis('equal')
    if len(iq) > 1 or len(labels) > 0:
        plt.legend()
    fancy = False
    if fancy:
        plt.gca().axhline(y=0, linewidth=2)
        plt.gca().axvline(x=0, linewidth=2)

    return fig
