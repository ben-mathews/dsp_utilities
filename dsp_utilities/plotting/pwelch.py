import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import welch

def pwelch(iq, fs_hz=1.0, t_sec = [], labels=[], title_append = '', nfft = 512, fig=None):
    """
    Simple time and frequency domain plots for IQ data

    :param numpy.ndarray iq: Array of IQ data
    :param float fs_hz: Sample rate in Hzpl
    :return: figure and axes objects

    """
    assert ((type(iq) is list) and (len(set(([type(e) for e in iq])))==1) and (list(set(([type(e) for e in iq])))[0] is np.ndarray)) or type(iq) is np.ndarray, "iq must be of type numpy.ndarray or list of numpy.ndarray arrays"
    if not (type(labels) is list and not labels): # Don't check labels if it is an empty list
        if type(iq) is list:
            assert ((type(labels) is list) and (len(set(([type(e) for e in labels])))==1) and (list(set(([type(e) for e in labels])))[0] is str)), "If type(iq) is list, then labels must be a list of strings"
        elif type(iq) is np.ndarray:
            assert type(labels) is str, "If type(iq) is numpy.ndarray, then labels must be a string or a list of strings"
        else:
            raise AssertionError('If type(iq) is list, then labels must be a list of strings.  If type(iq) is numpy.ndarray, then labels must be a string or a list of strings.  Neither of these conditions have been met')

    is_ndarray = False
    if type(iq) is np.ndarray:
        is_ndarray = True
        iq = [iq]
        if type(fs_hz) is float:
            fs_hz = [fs_hz]
        else:
            raise AssertionError('Expecting fs_hz to be type float')
        if type(labels) is str:
            labels = [labels]

    if type(fs_hz) is not list:
        fs_hz = [fs_hz] * len(iq)

    if len(t_sec) == len(iq[0]):
        t_sec = len(iq)*[t_sec]

    if t_sec:
        if len(iq) > 1 and len(t_sec) > 1 and len(t_sec) != len(iq):
            raise AssertionError('Length of t list must either equal 1, or the length of the iq list.')
        if len(iq) > 1 and len(t_sec) > 1:
            for idx, (iq_, t_) in enumerate(zip(iq, t_sec)):
                assert len(iq_) == len(t_), 'len(iq[{0}]) should be equal to len(t[{0}])'.format(idx)
        #if len(t)==1 and len(t[0]) < np.min([len(x) for x in iq])

    iq_dims = np.shape(iq)

    if fig is None:
        fig = plt.figure(figsize=(7.0, 9.5))
    else:
        plt.figure(fig.number)
    ax = plt.gca()

    # Find the max number of elements in the IQ vectors
    lens = [len(x) for x in iq]
    if len(np.unique(lens)) > 1:
        print('Warning, vectors are of different lengths.  Using the shortest length')
        num_elements = np.min(lens)
    else:
        num_elements = lens[0]

    for idx, (iq_, fs_hz_) in enumerate(zip(iq, fs_hz)):
        if len(labels) == 0:
            label = 'Trace {}'.format(idx)
        else:
            label = labels[idx]

        if not t_sec:
            t_sec_ = np.linspace(0, stop=num_elements, num=num_elements) / fs_hz_
        else:
            t_sec_ = t_sec[idx]

        f_welch, s_welch = welch(iq_, fs_hz_, 'hann', nfft, return_onesided=False, detrend=False)
        f_welch = np.fft.fftshift(f_welch)
        s_welch = np.fft.fftshift(10 * np.log10(s_welch))

        if is_ndarray:
            ax.plot(f_welch, s_welch)
            ax.grid()
            ax.set_ylim([10 * np.ceil((s_welch.min() - 10) / 10), 10 * np.floor((s_welch.max() + 10) / 10)])
            ax.set_xlabel('Frequency (Hz)')
            ax.set_ylabel('Power (dB)')
            ax.set_title('Welch Power Spectra' + title_append)

        else:
            ax.plot(f_welch, s_welch, label='{} PSD'.format(label))

            ax.grid()
            # ax.set_ylim([10 * np.ceil((s_welch.min() - 10) / 10), 10 * np.floor((s_welch.max() + 10) / 10)])
            ax.set_xlabel('Frequency (Hz)')
            ax.set_ylabel('Power (dB)')
            ax.set_title('Welch Power Spectra' + title_append)
            ax.legend()

    fig.tight_layout()
    fig.show()

    return fig, ax