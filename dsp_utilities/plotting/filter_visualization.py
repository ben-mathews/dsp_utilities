import numpy as np
import matplotlib.pyplot as plt

def mfreqz(b,a=1):
    from scipy import signal
    w,h = signal.freqz(b,a)
    h_dB = 20 * np.log10 (np.abs(h))
    plt.figure(figsize=(7.0, 4.75))
    plt.subplot(211)
    plt.plot(w/max(w),h_dB)
    plt.ylim(-150, 5)
    plt.ylabel('Magnitude (db)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Frequency response')
    plt.grid(True)
    plt.subplot(212)
    h_Phase = np.unwrap(np.arctan2(np.imag(h),np.real(h)))
    plt.plot(w/max(w),h_Phase)
    plt.ylabel('Phase (radians)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Phase response')
    plt.grid(True)
    plt.subplots_adjust(hspace=0.5)
    plt.tight_layout()

def mfreqz_multiple(b,a, labels):
    from scipy import signal
    fig, ax = plt.subplots(2, 1, figsize=(7.0, 9.5))

    for idx, (b_, a_, label_) in enumerate(zip(b,a,labels)):
        w,h = signal.freqz(b_,a_)
        h_dB = 20 * np.log10 (np.abs(h))
        h_Phase = np.unwrap(np.arctan2(np.imag(h), np.real(h)))

        ax[0].plot(w / max(w), h_dB, label=label_)
        ax[1].plot(w / max(w), h_Phase, label=label_)

    ax[0].set_ylabel('Magnitude (db)')
    ax[0].set_xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    ax[0].set_title(r'Frequency response')
    ax[0].grid()
    ax[0].legend()

    ax[1].set_ylabel('Phase (radians)')
    ax[1].set_xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    ax[1].set_title(r'Phase response')
    ax[1].grid()
    ax[1].legend()
    plt.tight_layout()

def filter_response(taps, labels=[]):
    if type(taps) is np.ndarray:
        taps = [taps]

    fig, ax = plt.subplots(2, 1, figsize=(7.0, 9.5))

    for idx, (taps_,) in enumerate(zip(taps)):
        if len(labels) == 0:
            label = 'Trace {}'.format(idx)
        else:
            label = labels[idx]
        x = np.linspace(-int(len(taps_) / 2), int(len(taps_) / 2 - 1), len(taps_))
        ax[0].plot(x, np.real(taps_), label='{} (Real)'.format(label))
        ax[1].plot(x, np.imag(taps_), label='{} (Imag)'.format(label))

    ax[0].set_ylabel('Tap Value (Real)')
    ax[0].set_xlabel('Sample Value')
    ax[0].set_title(r'Real Filter Taps')
    ax[0].grid()
    ax[0].legend()

    ax[1].set_ylabel('Tap Value (Imag)')
    ax[1].set_xlabel('Sample Value')
    ax[1].set_title(r'Imag Filter Taps')
    ax[1].grid()
    ax[1].legend()
    plt.tight_layout()
