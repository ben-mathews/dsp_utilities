import numpy as np
import threading
import scipy

def psd(iq:np.ndarray, fs_hz:float = 1.0, nfft:int = 1024, window:str = 'hann', center_frequency_hz:float = 0.0,
        scale:str = 'log', multi_threaded:bool = False):
    """
    Computes and returns a Power Spectral Density (PSD) estimate

    Args:
        iq (np.ndarray): Numpy array of complex IQ data to compute the PSD on
        fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
        nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
        window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
        center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
        scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
        multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

    Returns:
        A tuple with:
            f_welch (np.ndarray): The frequency vector associated with the PSD results.  Centered at
                                center_frequency_hz.
            s_welch (np.ndarray): The PSD results.

    """
    if multi_threaded:
        f_welch, s_welch = __welch_parallel__(iq, fs_hz, window, nfft, return_onesided=False, detrend=False,
                                              num_threads=4)
    else:
        f_welch, s_welch = scipy.signal.welch(iq, fs_hz, window, nfft, return_onesided=False, detrend=False)

    f_welch = center_frequency_hz + np.fft.fftshift(f_welch)
    if scale == 'log':
        s_welch = np.fft.fftshift(10 * np.log10(s_welch))

    return f_welch, s_welch


def __welch_parallel__(x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant',
                       return_onesided=True, scaling='density', axis=-1, average='mean', num_threads=4):
    """
    Internal function to perform parallelized welch transform.  Function parameters match scipy.signal.welch.

    """
    f = []
    Pxx = []
    for block_num in range(0, num_threads):
        f.append(np.array([]))
        Pxx.append(np.array([]))
    welch_block_size = int(len(x) / num_threads)

    welch_threads = []
    for thread_index in range(0, num_threads):
        welch_threads.append(
            threading.Thread(
                target=__welch_parallel_helper_function__,
                args=(f, Pxx, thread_index,
                      x[(thread_index * welch_block_size):((thread_index + 1) * welch_block_size)],
                      fs, window, nperseg, noverlap, nfft, detrend, return_onesided, scaling, axis, average)))
    for welch_thread in welch_threads:
        welch_thread.start()
    for welch_thread in welch_threads:
        welch_thread.join()
    Pxx = np.mean(Pxx, 0)
    f = f[0]

    return f, Pxx


def __welch_parallel_helper_function__(f, Pxx, index, x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None,
                                       detrend='constant', return_onesided=True, scaling='density', axis=-1,
                                       average='mean'):
    """
    Helper function for parallelized welch function.

    """
    f[index], Pxx[index] = \
        scipy.signal.welch(x, fs, window, nperseg, noverlap, nfft, detrend, return_onesided, scaling, axis, average)

