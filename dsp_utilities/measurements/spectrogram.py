import numpy as np
import scipy
from scipy.signal.spectral import _triage_segments
import threading

def spectrogram(iq:np.ndarray, fs_hz:float = 1.0, nfft:int = 1024, nperseg:int = 1024, noverlap:int = 128,
                window:str = 'hann', center_frequency_hz:float = 0.0, scale:str = 'log',
                multi_threaded:bool = False):
    """
    Computes and returns a spectrogram (aka waterfall) estimate

    Args:
        iq (np.ndarray): Numpy array of complex IQ data to compute the spectrogram on
        fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
        nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
        nperseg (int, optional): Number of samples per segment.  Defaults to 1024.
        noverlap (int, optional): Number of samples of overlap between segments.  Defaults to 128.
        window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
        center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
        scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
        multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

    Returns:
        A tuple with:
            f (np.ndarray): The frequency vector associated with the spectrogram results.  Centered at
                            center_frequency_hz.
            t (np.ndarray): The time vector associated with the spectrogram results.  Starts at 0.0.
            sxx (np.ndarray): The spectrogram PSD results.

    """
    if multi_threaded and len(iq) > 1000000:
        f, t, sxx = __spectrogram_parallel__(iq, fs_hz, window=window, nperseg=nperseg, noverlap=noverlap, nfft=nfft,
                                             return_onesided=False, mode='psd', num_threads=8)
    else:
        f, t, sxx = scipy.signal.spectrogram(iq, fs_hz, window=window, nperseg=nperseg, noverlap=noverlap, nfft=nfft,
                                             return_onesided=False, mode='psd')

    sxx = np.fft.fftshift(sxx, 0)
    if scale == 'log':
        sxx = 10 * np.log10(sxx)

    f = np.fft.fftshift(f)
    f = center_frequency_hz + f

    return f, t, sxx


def __spectrogram_parallel__(x, fs=1.0, window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant',
                             return_onesided=True, scaling='density', axis=-1, mode='psd', num_threads=4):
    """
    Internal function to perform parallelized spectrograms.  Function parameters match scipy.signal.spectrogram.

    """

    window_, nperseg = _triage_segments(window, nperseg, input_length=x.shape[axis])

    if noverlap is None:
        noverlap = nperseg // 8

    if nfft is None:
        nfft = nperseg
    elif nfft < nperseg:
        raise ValueError('nfft must be greater than or equal to nperseg.')
    else:
        nfft = int(nfft)

    nstep = nperseg - noverlap
    sxx_shape = (nperseg, (len(x)-noverlap)//nstep)
    sxx_combined = np.empty(sxx_shape)
    num_spectrogram_blocks = num_threads
    num_segments_per_block = sxx_shape[1] // num_spectrogram_blocks
    num_samples_per_block = nstep*num_segments_per_block + noverlap

    f = []
    t = []
    segment_start_index = []
    segment_end_index = []
    sample_start_index = []
    sample_end_index = []
    for block in range(0, num_spectrogram_blocks):
        if block == num_spectrogram_blocks - 1:
            sample_start_index.append(block * nstep * num_segments_per_block)
            sample_end_index.append(len(x))
        else:
            sample_start_index.append(block * nstep * num_segments_per_block)
            sample_end_index.append(block * nstep * num_segments_per_block + num_samples_per_block)
        segment_start_index.append(block * num_segments_per_block)
        segment_end_index.append(int((sample_end_index[-1]+1-noverlap)/nstep))
        t.append(np.asarray([]))
        f.append(np.asarray([]))

    spectrogram_threads = []#
    for thread_index in range(0, num_threads):
        spectrogram_threads.append(
            threading.Thread(target=__spectrogram_parallel_helper_function__,
                             args=(f, t, sxx_combined, thread_index, segment_start_index[thread_index],
                                   segment_end_index[thread_index],
                                   x[sample_start_index[thread_index]:sample_end_index[thread_index]],
                                   fs, window, nperseg, noverlap, nfft, detrend, return_onesided, scaling, axis, mode)))

    for welch_thread in spectrogram_threads:
        welch_thread.start()
    for welch_thread in spectrogram_threads:
        welch_thread.join()

    f_combined = f[0]
    for t_index in range(0, len(t)):
        t[t_index] = t[t_index] + sample_start_index[t_index]/fs
    t_combined = np.concatenate(t)
    sxx_combined

    return f_combined, t_combined, sxx_combined


def __spectrogram_parallel_helper_function__(f, t, sxx, index, segment_start_index, segment_end_index, x, fs=1.0,
                                             window='hann', nperseg=None, noverlap=None, nfft=None, detrend='constant',
                                             return_onesided=True, scaling='density', axis=-1, mode='psd'):
    """
        Helper function for parallelized spectrogram function.

    """
    f[index], t[index], sxx[:, segment_start_index:segment_end_index] = \
        scipy.signal.spectrogram(x=x, fs=fs, window=window, nperseg=nperseg, noverlap=noverlap, nfft=nfft,
                                 detrend=detrend, return_onesided=return_onesided, scaling=scaling, axis=axis,
                                 mode=mode)

