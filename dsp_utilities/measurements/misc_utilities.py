import numpy as np

def c_plus_n_over_n_to_c_over_n(c_plus_n_over_n:float):
    """
    Converts from C+N/N (as would be measured by using delta markers on a spectrum analyzer) to C/N
    Args:
        c_plus_n_over_n (float): C+N/N in dB

    Returns:
        c_over_n_log (float): C/N in dB

    """
    c_plus_n_over_n_lin = 10**(c_plus_n_over_n/10)
    c_over_n_lin = c_plus_n_over_n_lin - 1
    c_over_n_log = 10*np.log10(c_over_n_lin)
    return c_over_n_log

def c_over_n_to_esn0(c_over_n:float, fsym_hz:float, bw_hz:float):
    """
    Converts from C/N to an estimated Es/N0
    Args:
        c_over_n (float): C/N in dB
        fsym_hz (float): Symbol rate in Hz
        bw_hz (float): Bandwidth in Hz

    Returns:
        esn0_log: Estimated Es/N0 in dB

    """
    c_over_n_lin = 10**(c_over_n/10)
    esn0_lin = c_over_n_lin * (bw_hz/fsym_hz)
    esn0_log = 10*np.log10(esn0_lin)
    return esn0_log
