import fractions
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import resample_poly, upfirdn
from dsp_utilities.utilities.enuerations import ModulationType
from digital_demod.utilities.rrc_filter import rrc_filter
from dsp_utilities.plotting.iq_plot import iq_plot
from dsp_utilities.plotting.scatterplot import scatterplot

def generate_carrier_iq(cf_hz, fsym_hz, mod_type, rrc_beta, power, fs_hz, num_samples):
    oversample_factor = int(np.ceil(fs_hz / fsym_hz))
    fs_temp_hz = fsym_hz * oversample_factor
    rrc_length = 64 * oversample_factor
    h_rrc, time_idx = rrc_filter(rrc_length, rrc_beta, fsym_hz, fs_temp_hz)
    # h_rrc = h_rrc / np.sum(h_rrc)

    num_symbols = int(np.ceil(num_samples / fs_hz * fsym_hz) + len(h_rrc))

    if mod_type is ModulationType.MOD_BPSK:
        bits_per_symbol = 1
    elif mod_type is ModulationType.MOD_QPSK:
        bits_per_symbol = 2
    elif mod_type is ModulationType.MOD_8PSK:
        bits_per_symbol = 3
    else:
        raise Exception('Unhandled Modulation Type')

    data = np.random.randint(0, 2**bits_per_symbol, num_symbols)

    if mod_type is ModulationType.MOD_BPSK or mod_type is ModulationType.MOD_QPSK or \
            mod_type is ModulationType.MOD_8PSK:
        symbols = np.exp(1j * data / 2 ** bits_per_symbol * 2 * np.pi)
    if mod_type is ModulationType.MOD_QPSK:
        symbols = symbols * np.exp(1j * np.pi / 4)
    else:
        raise Exception('Unhandled Modulation Type')

    iq = upfirdn(h_rrc, symbols, up=oversample_factor, down=1)
    iq = iq[int(len(h_rrc) / 2):-int(len(h_rrc) / 2 - oversample_factor)]

    t = np.linspace(0, len(iq)-1, len(iq)) / fs_temp_hz
    iq = np.sqrt(power) * iq * np.exp(1j * 2 * np.pi * cf_hz * t)

    resample_frequency_frac = fractions.Fraction(fs_hz / fs_temp_hz).limit_denominator(10000)
    if resample_frequency_frac.numerator != resample_frequency_frac.denominator:
        iq = resample_poly(iq, resample_frequency_frac.numerator, resample_frequency_frac.denominator)
    iq = iq[0:num_samples]

    return iq




if __name__ == '__main__':
    fs_hz = 54e6
    num_samples = int(5.4e6)

    k = 1.380649e-23  # Boltzmann constant (thermal noise power in J/K)
    T = 293  # Thermal noise temp in deg Kelvin
    noise_var = k*T*fs_hz
    n = np.random.normal(0, np.sqrt(noise_var/2), num_samples) + 1j * np.random.normal(0, np.sqrt(noise_var/2), num_samples)

    carriers_to_generate = [
        {'cf_hz': -20e6, 'fsym_hz': 1e6, 'mod_type': ModulationType.MOD_QPSK, 'rrc_beta': 0.35, 'power': 1e-13, 'fs_hz': fs_hz, 'num_samples': num_samples},
        {'cf_hz': -10e6, 'fsym_hz': 2e6, 'mod_type': ModulationType.MOD_QPSK, 'rrc_beta': 0.35, 'power': 1e-13, 'fs_hz': fs_hz, 'num_samples': num_samples},
        {'cf_hz': 10e6,  'fsym_hz': 4e6, 'mod_type': ModulationType.MOD_QPSK, 'rrc_beta': 0.35, 'power': 1e-13, 'fs_hz': fs_hz, 'num_samples': num_samples},
    ]

    iq = np.zeros(num_samples) + 1j * np.zeros(num_samples)
    for car in carriers_to_generate:
        iq_ = generate_carrier_iq(car['cf_hz'], car['fsym_hz'], car['mod_type'], car['rrc_beta'], car['power'],
                                 car['fs_hz'], car['num_samples'])
        cn_db = 10 * np.log10( car['power'] / ((1+car['rrc_beta']/8)*car['fsym_hz'] / car['fs_hz']) )
        iq = iq + iq_

    iq = iq + n

    if False:
        iq_plot(iq[0:100000], fs_hz=fs_hz, nfft=512)

        from scipy.signal import welch
        nfft = 16384
        f_welch, s_welch = welch(iq_, fs_hz, 'hann', 16384, return_onesided=False, detrend=False)
        f_welch = np.fft.fftshift(f_welch)
        s_welch = np.fft.fftshift((fs_hz/nfft) * s_welch)
        plt.figure()
        plt.plot(f_welch, s_welch)
        #plt.plot(s_welch)

        np.sum(s_welch[59:75])
        np.sum(s_welch[1930:2316])
        np.sum(s_welch[1945:2300])