import os
from copy import deepcopy
import fractions
import importlib
import pkgutil

import json
import datetime as dt
import numpy as np
from scipy import signal
from scipy.io import loadmat, savemat

from .iq_file_plugin_bluefile import IQFile_Plugin_BlueFile
from .iq_file_plugin_raw import IQFile_Plugin_Raw
from .iq_file_plugin_vita49 import IQFile_Plugin_VITA49
from .iq_file_enumerations import IQDataFormat, IQDataOrder, IQFileType


class IQFile:
    def __init__(self, filename, file_type=None, iq_data_format=None, iq_data_order=IQDataOrder.IQ, iq=None, fs_hz=None,
                 t_start_sec=None, timecode_utc_sec=None, timecode_datetime=None, timecode_datetime_str=None,
                 center_frequency_hz=0.0, num_samples=None, header_ignore_bytes=0, variable_mapping={},
                 overrides_dict = {}):

        # If file type is RAW, certain things must be specified
        if file_type == IQFileType.RAW:
            required_keywords = IQFile_Plugin_Raw.required_keywords()
            for required_keyword in required_keywords:
                assert eval(required_keyword) is not None, \
                    '{} must be defined as a non-None value for file_type == {}'.format(required_keyword, file_type)
        elif file_type == IQFileType.MATFILE:
            assert 'iq' in variable_mapping, '\'iq\' must be in variable_mapping for file_type = {}'.format(file_type)
        self.filename = filename
        if file_type is not None:
            self.file_type = file_type
        else:
            # Guess file type from filename
            filename, file_extension = os.path.splitext(self.filename)
            file_extension = file_extension.lower()
            if file_extension == '.raw' or file_extension == '.dat' or file_extension == '.bin':
                self.file_type = IQFileType.RAW
            elif file_extension == '.tmp' or file_extension == '.det':
                self.file_type = IQFileType.BLUEFILE
            elif file_extension == '.npy':
                self.file_type = IQFileType.NPYFILE
            elif file_extension == '.npz':
                self.file_type = IQFileType.NPZ
            elif file_extension == '.mat':
                self.file_type = IQFileType.MATFILE
            elif file_extension == '.vita49':
                self.file_type = IQFileType.VITA49FILE
            else:
                raise Exception('Could not guess file extension from filename ({}).  It must be specified in constructor'.format(filename))

        self.iq_data_format = iq_data_format
        self.iq_data_order = iq_data_order
        self.iq = iq
        self.fs_hz = fs_hz
        self.t_start_sec = t_start_sec
        self.timecode_utc_sec = timecode_utc_sec
        self.timecode_datetime = timecode_datetime
        self.timecode_datetime_str = timecode_datetime_str
        self.center_frequency_hz = center_frequency_hz
        if num_samples is None and self.iq is not None:
            self.num_samples = len(self.iq)
        else:
            self.num_samples = num_samples
        self.header_ignore_bytes = header_ignore_bytes
        self.variable_mapping = variable_mapping
        self.t_iq_sec = None
        self.metadata_has_been_read = False

        self.last_read_start_sample = None
        self.last_read_end_sample = None

        if os.path.exists(self.filename): # If not, then we're writting a new file
            self.read_metadata(overrides_dict)


    @staticmethod
    def guess_file_type(filename):
        filename_without_extension, file_extension = os.path.splitext(filename)
        file_extension = file_extension.lower()
        if file_extension == '.raw' or file_extension == '.dat' or file_extension == '.bin':
            file_type = IQFileType.RAW
        elif file_extension == '.tmp' or file_extension == '.det':
            try:
                iq_file = IQFile(filename=filename, file_type=IQFileType.BLUEFILE)
            except Exception as ex:
                iq_file = None
                file_type = None
            if iq_file is not None:
                file_type = IQFileType.BLUEFILE
        elif file_extension == '.npy':
            file_type = IQFileType.NPYFILE
        elif file_extension == '.npz':
            file_type = IQFileType.NPZ
        elif file_extension == '.mat':
            file_type = IQFileType.MATFILE
        elif file_extension == '.vita49':
            try:
                iq_file = IQFile(filename=filename, file_type=IQFileType.VITA49FILE,
                                 overrides_dict = {'max_num_context_packets': 1})
            except Exception as ex:
                iq_file = None
                file_type = None
            if iq_file is not None:
                file_type = IQFileType.VITA49FILE
        else:
            file_type = None
        return file_type



    def sample_size_bytes(self):
        if self.iq_data_format == IQDataFormat.INT8:
            return 2
        elif self.iq_data_format == IQDataFormat.INT16:
            return 4
        elif self.iq_data_format == IQDataFormat.FLOAT32:
            return 8
        elif self.iq_data_format == IQDataFormat.DOUBLE64:
            return 16
        else:
            raise Exception('Unhandled file format: ' + str(self.self.file_format))


    def read_metadata(self, overrides_dict = {}):
        if self.file_type == IQFileType.RAW:
            iq_file_raw = IQFile_Plugin_Raw(filename=self.filename)
            metadata_dict = self.update_metadata_from_class()
            metadata_dict = iq_file_raw.read_metadata(metadata_dict=metadata_dict)
            self.update_class_from_metadata(metadata_dict)

        elif self.file_type == IQFileType.BLUEFILE:
            iq_file_bluefile = IQFile_Plugin_BlueFile(filename=self.filename)
            overrides_dict = {}
            metadata = iq_file_bluefile.read_metadata(metadata_dict=overrides_dict)
            self.update_class_from_metadata(metadata)

        elif self.file_type == IQFileType.NPYFILE:
            raise Exception('Need to implement')

        elif self.file_type == IQFileType.NPZFILE:
            raise Exception('Need to implement')

        elif self.file_type == IQFileType.MATFILE:
            ttt = 1
            mat_contents = loadmat(self.filename)
            iq = np.squeeze(mat_contents[self.variable_mapping['iq']])
            self.num_samples = len(iq)
            if type(iq[0]) is np.complex128:
                self.iq_data_format = IQDataFormat.DOUBLE64
                self.iq_data_order = IQDataOrder.IQ
            else:
                raise Exception('Unhandled data format: {}'.format(type(iq[0])))
            if self.fs_hz is None:
                if 'fs_hz' not in self.variable_mapping or \
                        self.variable_mapping['fs_hz'] not in mat_contents:
                    self.fs_hz = 1
                else:
                    self.fs_hz = float(np.squeeze(mat_contents[self.variable_mapping['fs_hz']]))
            if self.t_start_sec is None:
                if 't_start_sec' not in self.variable_mapping or \
                        self.variable_mapping['t_start_sec'] not in mat_contents:
                    self.t_start_sec = 0.0
                else:
                    self.t_start_sec = float(np.squeeze(mat_contents[self.variable_mapping['t_start_sec']]))
            if self.center_frequency_hz is None:
                if 'center_frequency_hz' not in self.variable_mapping or \
                        self.variable_mapping['center_frequency_hz'] not in mat_contents:
                    self.center_frequency_hz = 0.0
                else:
                    self.center_frequency_hz = float(np.squeeze(mat_contents[self.variable_mapping['center_frequency_hz']]))
        elif self.file_type == IQFileType.VITA49FILE:
            iq_file_vita49 = IQFile_Plugin_VITA49(filename=self.filename)
            metadata = iq_file_vita49.read_metadata(metadata_dict=overrides_dict)
            self.update_class_from_metadata(metadata)
        else:
            raise Exception('Unhandled file type: ' + str(self.file_type))
        self.metadata_has_been_read = True


    def update_class_from_metadata(self, metadata):
        self.filename = metadata['filename']
        self.center_frequency_hz = metadata['center_frequency_hz']
        self.header_ignore_bytes = metadata['header_ignore_bytes']
        self.num_samples = metadata['num_samples']
        self.fs_hz = metadata['fs_hz']
        self.t_start_sec = metadata['t_start_sec']
        self.timecode_utc_sec = metadata['timecode_utc_sec']
        self.timecode_datetime = metadata['timecode_datetime']
        self.timecode_datetime_str = metadata['timecode_datetime_str']
        self.iq_data_order = metadata['iq_data_order']
        self.iq_data_format = metadata['iq_data_format']


    def update_metadata_from_class(self):
        metadata = {
            'filename': self.filename,
            'center_frequency_hz': self.center_frequency_hz,
            'header_ignore_bytes': self.header_ignore_bytes,
            'num_samples': self.num_samples,
            'fs_hz': self.fs_hz,
            't_start_sec': self.t_start_sec,
            'timecode_utc_sec': self.timecode_utc_sec,
            'timecode_datetime': self.timecode_datetime,
            'timecode_datetime_str': self.timecode_datetime_str,
            'iq_data_order': self.iq_data_order,
            'iq_data_format': self.iq_data_format,
            'sample_size_bytes': self.sample_size_bytes()
        }
        return metadata


    def read_file(self, start_sample=None, end_sample=None):
        """
        Reads IQ data from file
        :param start: Starting index of data to read (0 based) - currently only implemented for BLUE and RAW files
        :param end: Ending index if data to read (0 based) - currently only implemented for BLUE and RAW files
        :return: void

        :note: Generates t vector after reading from file
        """
        if start_sample is None:
            start_sample = 0
        if end_sample is None:
            end_sample = self.num_samples - 1
        number_of_samples = end_sample - start_sample + 1

        self.last_read_start_sample = start_sample
        self.last_read_end_sample = end_sample

        if self.file_type == IQFileType.RAW:
            metadata_dict = self.update_metadata_from_class()
            iq_file_bluefile = IQFile_Plugin_Raw(filename=self.filename)
            self.iq = iq_file_bluefile.read_iq_data(start_sample=start_sample,
                                                    number_of_samples=number_of_samples,
                                                    metadata_dict=metadata_dict)

        elif self.file_type == IQFileType.BLUEFILE:
            metadata_dict = {}
            iq_file_bluefile = IQFile_Plugin_BlueFile(filename=self.filename)
            self.iq = iq_file_bluefile.read_iq_data(start_sample=start_sample,
                                                    number_of_samples=number_of_samples,
                                                    metadata_dict=metadata_dict)

        elif self.file_type == IQFileType.NPYFILE:
            raise Exception('Not yet implemented')

        elif self.file_type == IQFileType.NPZFILE:
            raise Exception('Not yet implemented')

        elif self.file_type == IQFileType.MATFILE:
            if self.num_samples is None or self.fs_hz is None or self.t_start_sec is None:
                self.read_metadata()
            mat_contents = loadmat(self.filename)
            self.iq = np.squeeze(mat_contents[self.variable_mapping['iq']])
        elif self.file_type == IQFileType.VITA49FILE:
            iq_file_vita49 = IQFile_Plugin_VITA49(filename=self.filename)
            metadata_dict = {}
            self.iq = iq_file_vita49.read_iq_data(start_sample=start_sample,
                                                  number_of_samples=number_of_samples,
                                                  metadata_dict=metadata_dict)
        else:
            raise Exception('Unhandled file type: ' + str(self.file_type))

        t_start_sec = None
        if start_sample is not None:
            t_start_sec = (start_sample-1) / self.fs_hz
        self.generate_t_vector(t_start_sec=t_start_sec)


    def write_file(self, file_type=None, filename=None):
        if file_type == None:
            file_type = self.file_type
        if filename == None:
            filename = self.filename

        if os.path.isfile(filename):
            os.remove(filename)

        if file_type == IQFileType.RAW:
            if self.iq_data_format == IQDataFormat.INT16:
                real_samples = np.empty((2 * len(self.iq),), dtype=np.int16)
                if self.iq_data_order == IQDataOrder.IQ:
                    real_samples[0::2] = np.real(self.iq).astype(np.int16)
                    real_samples[1::2] = np.imag(self.iq).astype(np.int16)
                else:
                    real_samples[0::2] = np.imag(self.iq).astype(np.int16)
                    real_samples[1::2] = np.real(self.iq).astype(np.int16)
            else:
                raise Exception('Unhandled file format type: ' + str(self.iq_data_format))
            real_samples.tofile(filename)

        elif file_type == IQFileType.BLUEFILE:
            iq_file_bluefile = IQFile_Plugin_BlueFile(filename=filename)
            metadata = self.update_metadata_from_class()
            metadata['filename'] = filename
            iq_file_bluefile.write_iq_data(self.iq, metadata=metadata)

        elif file_type == IQFileType.NPYFILE:
            raise Exception('Not yet implemented')

        elif file_type == IQFileType.MATFILE:
            mdic = {"iq": self.iq, "fs": self.fs_hz}
            savemat(filename, mdic, format='4', oned_as='column')

        elif file_type == IQFileType.NPZFILE:
            raise Exception('Not yet implemented')

        else:
            raise Exception('Unhandled file type: ' + str(file_type))


    def export_file(self, new_file_type, new_filename):
        self.write_file(file_type=new_file_type, filename=new_filename)


    def update_filename(self, filename):
        self.filename = filename


    def update_file_type(self, file_type):
        self.file_type = file_type
        """
        file_size_bytes = os.path.getsize(self.filename)
        num_samples = int((file_size_bytes - self.header_ignore_bytes) / self.sample_size_bytes())
        if num_samples < self.num_samples:
            self.num_samples = num_samples
        if self.iq is not None:
            self.read_file()
        if self.t_iq_sec is not None:
            self.generate_t_vector()
        """


    def update_iq_data_format(self, iq_data_format):
        self.iq_data_format = iq_data_format
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()
        if self.t_iq_sec is not None:
            self.generate_t_vector()


    def update_iq_data_order(self, iq_data_order):
        self.iq_data_order = iq_data_order
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()
        if self.t_iq_sec is not None:
            self.generate_t_vector()


    def update_sample_rate(self, fs_hz):
        self.fs_hz = fs_hz
        if self.t_iq_sec is not None:
            self.generate_t_vector()


    def update_start_time_sec(self, t_start_sec):
        self.t_start_sec = t_start_sec
        if self.t_iq_sec is not None:
            self.generate_t_vector()


    def update_start_time_dtg(self, start_time_dtg):
        self.timecode_datetime = start_time_dtg
        self.timecode_datetime_str = timecode_datetime_str = str(self.timecode_datetime).replace('T', ' ')


    def update_center_frequency_hz(self, center_frequency_hz):
        self.center_frequency_hz = center_frequency_hz


    def update_num_samples(self, num_samples):
        self.num_samples = num_samples
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()


    def update_header_ignore_bytes(self, header_ignore_bytes):
        self.header_ignore_bytes = header_ignore_bytes
        #if self.iq is not None and os.path.isfile(self.filename):
        #    self.read_file()


    def generate_t_vector(self, t_start_sec = None):
        if t_start_sec is None:
            t_start_sec = self.t_start_sec
        self.t_iq_sec = t_start_sec + np.linspace(0, len(self.iq) - 1, len(self.iq)) / self.fs_hz


    def tune(self, fc_hz):
        if self.t_iq_sec is None:
            self.generate_t_vector()

        assert len(self.t_iq_sec)==len(self.iq), 'IQ and t vectors are not the same length'

        complex_exponential = np.exp(-1j * 2 * np.pi * fc_hz * self.t_iq_sec)
        iq_tuned = self.iq * complex_exponential
        return iq_tuned


    def low_pass_filter(self, cutoff_frequency_hz, numtaps=271, window='hamming'):
        b = signal.firwin(numtaps=numtaps, cutoff=cutoff_frequency_hz/self.fs_hz, window=window)
        iq_filtered = signal.filtfilt(b, [1], self.iq)
        return iq_filtered


    def resample(self, new_fs_hz, denominator_limit=1000000):
        resample_frequency_frac = fractions.Fraction(new_fs_hz / self.fs_hz).limit_denominator(denominator_limit)
        iq_resampled = signal.resample_poly(self.iq, resample_frequency_frac.numerator, resample_frequency_frac.denominator)
        return iq_resampled, resample_frequency_frac


    def time_filter(self, start_time_sec, end_time_sec):
        if self.t_iq_sec is None:
            self.generate_t_vector()
        iq_filtered = self.iq[(self.t_iq_sec >= start_time_sec) & (self.t_iq_sec <= end_time_sec)]
        t_filtered = self.t_iq_sec[(self.t_iq_sec >= start_time_sec) & (self.t_iq_sec <= end_time_sec)]
        return t_filtered, iq_filtered
