from abc import ABC, abstractmethod

class IQFile_Plugin_ABC(ABC):

    @abstractmethod
    def __init__(self, filename):
        self.filename = filename
        pass


    @staticmethod
    @abstractmethod
    def filestypes_and_descriptions():
        pass


    @staticmethod
    @abstractmethod
    def required_keywords():
        pass


    @abstractmethod
    def autodetect(self):
        pass


    @abstractmethod
    def read_metadata(self, metadata_dict):
        pass


    @abstractmethod
    def read_iq_data(self, start_sample: int, number_of_samples: int, metadata_dict: dict):
        pass


    @abstractmethod
    def write_iq_data(self, iq, metadata):
        pass