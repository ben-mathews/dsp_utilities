from enum import Enum
import datetime as dt
import json

import numpy as np

from .iq_file_plugin_abc import IQFile_Plugin_ABC
from .iq_file_enumerations import IQDataFormat, IQDataOrder, IQFileType

from dsp_utilities.file_handlers import bluefile


class IQFile_Plugin_BlueFile(IQFile_Plugin_ABC):

    def __init__(self, filename):
        super().__init__(filename)


    @staticmethod
    def filestypes_and_descriptions():
        return [('BlueFile', '.tmp')]


    @staticmethod
    def required_keywords():
        return []


    def autodetect(self):
        print('')


    def read_metadata(self, metadata_dict):
        hdr = bluefile.readheader(self.filename, ext_header_type=str)
        if 'ext_header' in hdr and len(hdr['ext_header']) > 0:
            try:
                hdr['ext_header'] = json.loads(hdr['ext_header'])
            except Exception as e:
                print(e)
        if type(hdr['ext_header']) is dict and 'center_frequency_hz' in hdr['ext_header']:
            center_frequency_hz = hdr['ext_header']['center_frequency_hz']
        else:
            center_frequency_hz = 0.0
        header_ignore_bytes = 0
        num_samples = hdr['size']
        fs_hz = 1.0 / hdr['xdelta']
        t_start_sec = hdr['xstart']
        timecode_utc_sec = hdr['timecode']
        timecode_datetime = np.datetime64(dt.datetime(1950, 1, 1, 0, 0, 0) + dt.timedelta(seconds=hdr['timecode']))
        timecode_datetime_str = str(timecode_datetime).replace('T', ' ')
        iq_data_order = IQDataOrder.IQ
        if hdr['format'] == 'CB':
            iq_data_format = IQDataFormat.INT8
        elif hdr['format'] == 'CI':
            iq_data_format = IQDataFormat.INT16
        elif hdr['format'] == 'CF':
            iq_data_format = IQDataFormat.FLOAT32
        elif hdr['format'] == 'CD':
            iq_data_format = IQDataFormat.DOUBLE64
        else:
            raise Exception('Unhandled BlueFile format: ' + hdr['format'])

        metadata = {
            'filename': self.filename,
            'center_frequency_hz': center_frequency_hz,
            'header_ignore_bytes': header_ignore_bytes,
            'num_samples': num_samples,
            'fs_hz': fs_hz,
            't_start_sec': t_start_sec,
            'timecode_utc_sec': timecode_utc_sec,
            'timecode_datetime': timecode_datetime,
            'timecode_datetime_str': timecode_datetime_str,
            'iq_data_order': iq_data_order,
            'iq_data_format': iq_data_format
        }

        return metadata


    def read_iq_data(self, start_sample: int, number_of_samples: int, metadata_dict: dict):
        hdr, iq_array = bluefile.read(self.filename, start=start_sample + 1, end=start_sample + number_of_samples,
                                      ext_header_type=str)
        if 'ext_header' in hdr and len(hdr['ext_header']) > 0:
            try:
                hdr['ext_header'] = json.loads(hdr['ext_header'])
            except Exception as e:
                print('Exception reading extended_header: {}'.format(e))
        if type(hdr['ext_header']) is dict and 'center_frequency_hz' in hdr['ext_header']:
            self.center_frequency_hz = hdr['ext_header']['center_frequency_hz']
        else:
            self.center_frequency_hz = 0.0
        if hdr['format'][0] == 'C':
            if type(iq_array[0]) == np.complex64:  # CF
                iq = iq_array
            if type(iq_array[0]) == np.complex128:  # CD
                iq = iq_array
            elif type(iq_array[0]) == np.ndarray:
                iq = np.double(iq_array[:, 0]) + 1j * np.double(iq_array[:, 1])
            else:
                raise Exception('Unhandled data type: {}'.format(type(iq_array[0])))
        else:
            raise Exception('Expecting complex data type.  Read type: {}'.format(hdr['format']))
        return iq


    def write_iq_data(self, iq, metadata):
        if metadata['iq_data_format'] == IQDataFormat.INT16:
            format = 'CI'
            data_size = 2 * metadata['num_samples'] * np.dtype(np.int16).itemsize
        elif metadata['iq_data_format'] == IQDataFormat.FLOAT32:
            format = 'CF'
            data_size = 2 * metadata['num_samples'] * np.dtype(np.float32).itemsize
        elif metadata['iq_data_format'] == IQDataFormat.DOUBLE64:
            format = 'CD'
            data_size = 2 * metadata['num_samples'] * np.dtype(np.float64).itemsize
        else:
            raise Exception('Unhandled file format type: ' + str(metadata['iq_data_format']))

        xstart = metadata['t_start_sec']
        if xstart is None:
            xstart = 0.0
        
        num_samples = metadata['num_samples']
        if num_samples is None:
            num_samples = len(iq)

        hdr = bluefile.header(type=1000, format=format, detached=0, xstart=xstart,
                              size=num_samples, data_size=data_size, data_start=0,
                              xdelta=1.0 / metadata['fs_hz'])

        if metadata['center_frequency_hz'] is not None:
            hdr['ext_header'] = json.dumps({'center_frequency_hz': metadata['center_frequency_hz']})
        else:
            hdr['ext_header'] = ''

        if format == 'CF' or format == 'CD':
            bluefile.write(filename=metadata['filename'], hdr=hdr, ext_header_type=type(hdr['ext_header']), data=iq)
        else:
            bluefile.write(filename=metadata['filename'], hdr=hdr, ext_header_type=type(hdr['ext_header']),
                           data=np.column_stack((np.real(iq), np.imag(iq))))
