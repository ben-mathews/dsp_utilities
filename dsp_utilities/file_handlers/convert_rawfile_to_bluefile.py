from dsp_utilities.file_handlers.iq_file import IQFile, IQFileType, IQDataFormat, IQDataOrder

def convert_rawfile_to_bluefile(filename, new_filename, fs_hz, t_start_sec=0.0, center_frequency_hz=0.0):
    iq_file = IQFile(filename=filename, file_type=IQFileType.RAW, iq_data_format=IQDataFormat.FLOAT32,
                     iq_data_order=IQDataOrder.IQ, fs_hz=fs_hz, t_start_sec=t_start_sec, header_ignore_bytes=0,
                     center_frequency_hz=center_frequency_hz)
    iq_file.read_metadata()
    iq_file.read_file()
    iq_file.export_file(new_file_type=IQFileType.BLUEFILE, new_filename=new_filename)
