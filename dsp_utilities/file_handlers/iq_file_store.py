import numpy as np
from dsp_utilities.file_handlers import IQFile, IQFileType

class IQFileStore():
    def __init__(self, filename, file_type=IQFileType.BLUEFILE, iq_data_format=None, fs_hz=None, t_start_sec=None):
        if file_type == IQFileType.RAW:
            assert fs_hz is not None, 'fs_hz must be specified for RAW files'
        self.iq_file = IQFile(filename=filename, file_type=file_type, iq_data_format=iq_data_format, fs_hz=fs_hz, t_start_sec=t_start_sec)
        self.iq_file.read_metadata()

        if file_type == IQFileType.RAW:
            self.fs_hz = fs_hz
        elif file_type == IQFileType.BLUEFILE:
            self.fs_hz = self.iq_file.fs_hz
        else:
            raise('Unhandled file type')
        self.num_samples = self.iq_file.num_samples
        self.t_start_sec = self.iq_file.t_start_sec
        self.t_end_sec = self.iq_file.t_start_sec + self.num_samples / self.fs_hz


        self.iq = None
        self.t_sec = None


    def read_file(self, t_start_sec=0, t_end_sec=None, duration_sec=None, num_samples=None):
        assert t_start_sec >= self.t_start_sec and t_start_sec <= self.t_end_sec, 't_start_sec must be greater than the starting time of the IQ file and less than the ending time'
        assert t_end_sec is None or duration_sec is None, 'Cannot specify both t_end_sec and duration'
        if duration_sec is not None:
            t_end_sec = t_start_sec + duration_sec
        if num_samples is not None:
            t_end_sec = t_start_sec + num_samples / self.fs_hz
        assert t_end_sec  is not None and t_end_sec <= self.t_end_sec, 't_end_sec must be less than the ending time'

        start_index = np.int(np.floor(t_start_sec * self.fs_hz))
        end_index = np.int(np.ceil(t_end_sec * self.fs_hz))

        self.iq_file.read_file(start=start_index, end=end_index)

        if False:
            print('Debugging code to add frequency shift ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ')
            print('Debugging code to add frequency shift ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ')
            print('Debugging code to add frequency shift ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- ')
            from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_frequency_shift
            self.iq_file.iq = carrier_frequency_shift(iq=self.iq_file.iq, frequency_hz=1359.0, fs_hz=self.fs_hz)

        t_start_actual_sec = start_index / self.fs_hz
        t_end_actual_sec = end_index / self.fs_hz

        return t_start_actual_sec, t_end_actual_sec


    def get_samples(self, t_start_sec=0, t_end_sec=None, duration_sec=None, num_samples=None):
        assert t_start_sec >= self.t_start_sec and t_start_sec <= self.t_end_sec, 't_start_sec must be greater than the starting time of the IQ file and less than the ending time'
        assert t_end_sec is None or duration_sec is None, 'Cannot specify both t_end_sec and duration'
        if duration_sec is not None:
            t_end_sec = t_start_sec + duration_sec
        assert t_end_sec < self.t_end_sec, 't_end_sec must be less than the ending time'

        start_index = np.nanargmin(np.where(self.iq_file.t_sec > t_start_sec))
        end_index = np.nanargmin(np.where(self.iq_file.t_sec > t_end_sec))

        return self.iq_file.iq[start_index:end_index], self.iq_file.t_sec[start_index:end_index],

