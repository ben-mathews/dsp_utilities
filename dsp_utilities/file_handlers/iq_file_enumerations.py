from enum import Enum


class IQFileType(Enum):
    RAW = 1
    BLUEFILE = 2
    NPYFILE = 3
    NPZFILE = 4
    MATFILE = 5
    VITA49FILE = 6


class IQDataFormat(Enum):
    NA = 0
    INT8 = 1
    INT10 = 2
    INT12 = 3
    INT16 = 4
    FLOAT32 = 5
    DOUBLE64 = 6


class IQDataOrder(Enum):
    NA = 0
    IQ = 1
    QI = 2
