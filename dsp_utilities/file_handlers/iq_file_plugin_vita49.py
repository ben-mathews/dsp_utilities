from .iq_file_plugin_abc import IQFile_Plugin_ABC
from .iq_file_enumerations import IQDataFormat, IQDataOrder, IQFileType


class IQFile_Plugin_VITA49(IQFile_Plugin_ABC):

    def __init__(self, filename):
        super().__init__(filename)


    @staticmethod
    def filestypes_and_descriptions():
        return [('VITA49', '.vita49')]


    @staticmethod
    def required_keywords():
        return []


    def autodetect(self):
        print('')


    def read_metadata(self, metadata_dict):
        raise Exception('Need to implement')


    def read_iq_data(self, start_sample: int, number_of_samples: int, metadata_dict: dict):
        raise Exception('Need to implement')


    def write_iq_data(self, iq, metadata):
        raise Exception('Need to implement')