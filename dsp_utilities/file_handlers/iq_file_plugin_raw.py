from enum import Enum
import os
import json

import numpy as np

from .iq_file_plugin_abc import IQFile_Plugin_ABC
from .iq_file_enumerations import IQDataFormat, IQDataOrder, IQFileType


class IQFile_Plugin_Raw(IQFile_Plugin_ABC):

    def __init__(self, filename):
        super().__init__(filename)


    @staticmethod
    def filestypes_and_descriptions():
        return [('.dat files', '.dat'), ('.raw files', '.raw'), ('.bin files', '.bin')]


    @staticmethod
    def required_keywords():
        required_override_keys = [
            'iq_data_format',
            'iq_data_order',
            'header_ignore_bytes'
        ]
        return required_override_keys


    def autodetect(self):
        print('')


    def read_metadata(self, metadata_dict):
        file_size_bytes = os.path.getsize(self.filename)
        num_samples = int((file_size_bytes - metadata_dict['header_ignore_bytes']) / metadata_dict['sample_size_bytes'])

        metadata = {
            'filename': self.filename,
            'center_frequency_hz': metadata_dict['center_frequency_hz'],
            'header_ignore_bytes': metadata_dict['header_ignore_bytes'],
            'num_samples': num_samples,
            'fs_hz': metadata_dict['fs_hz'],
            't_start_sec': metadata_dict['t_start_sec'],
            'timecode_utc_sec': metadata_dict['timecode_utc_sec'],
            'timecode_datetime': metadata_dict['timecode_datetime'],
            'timecode_datetime_str': metadata_dict['timecode_datetime_str'],
            'iq_data_order': metadata_dict['iq_data_order'],
            'iq_data_format': metadata_dict['iq_data_format'],
            'sample_size_bytes': metadata_dict['sample_size_bytes']
        }

        return metadata


    def read_iq_data(self, start_sample: int, number_of_samples: int, metadata_dict: dict):
        assert 'iq_data_format' in metadata_dict, 'Expecting iq_data_format to be in metadata_dict'
        assert 'iq_data_order' in metadata_dict, 'Expecting iq_data_order to be in metadata_dict'
        assert 'header_ignore_bytes' in metadata_dict, 'Expecting header_ignore_bytes to be in metadata_dict'

        if metadata_dict['iq_data_format'] == IQDataFormat.INT8:
            dtype = np.int8
        elif metadata_dict['iq_data_format'] == IQDataFormat.INT16:
            dtype = np.int16
        elif metadata_dict['iq_data_format'] == IQDataFormat.FLOAT32:
            dtype = np.float32
        elif metadata_dict['iq_data_format'] == IQDataFormat.DOUBLE64:
            dtype = np.double
        else:
            raise Exception('Unhandled file format type: ' + str(metadata_dict['iq_data_format']))

        real_samples = \
            np.fromfile(
                file=self.filename, dtype=dtype, offset=int(start_sample * 2 * np.dtype(dtype).itemsize),
                count=int(2 * (number_of_samples) + metadata_dict['header_ignore_bytes'] / np.dtype(dtype).itemsize)
            )

        # Convert to complex and return
        if metadata_dict['iq_data_order'] == IQDataOrder.IQ:
            iq = real_samples[0::2] + 1j * real_samples[1::2]
        else:
            iq = real_samples[1::2] + 1j * real_samples[0::2]

        return iq


    def write_iq_data(self, iq, metadata):
        if metadata['iq_data_format'] == IQDataFormat.INT16:
            format = 'CI'
            data_size = 2 * self.num_samples * np.dtype(np.int16).itemsize
        elif metadata['iq_data_format'] == IQDataFormat.FLOAT32:
            format = 'CF'
            data_size = 2 * self.num_samples * np.dtype(np.float32).itemsize
        elif metadata['iq_data_format'] == IQDataFormat.DOUBLE64:
            format = 'CD'
            data_size = 2 * metadata['num_samples'] * np.dtype(np.float64).itemsize
        else:
            raise Exception('Unhandled file format type: ' + str(metadata['iq_data_format']))

        hdr = bluefile.header(type=1000, format=format, detached=0, xstart=metadata['t_start_sec'],
                              size=metadata['num_samples'], data_size=data_size, data_start=0,
                              xdelta=1.0 / metadata['fs_hz'])

        if metadata['center_frequency_hz'] is not None:
            hdr['ext_header'] = json.dumps({'center_frequency_hz': metadata['center_frequency_hz']})
        else:
            hdr['ext_header'] = ''

        if format == 'CF' or format == 'CD':
            bluefile.write(filename=metadata['filename'], hdr=hdr, ext_header_type=type(hdr['ext_header']), data=iq)
        else:
            bluefile.write(filename=metadata['filename'], hdr=hdr, ext_header_type=type(hdr['ext_header']),
                           data=np.column_stack((np.real(iq), np.imag(iq))))
