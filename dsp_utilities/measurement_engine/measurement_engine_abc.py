from abc import ABC, abstractmethod

class MeasurementEngine_Plugin_ABC(ABC):
    """Abstract Base Class interface for a measurement engine for performing basic spectrum and carrier measurements

    This class defines the measurement engine described Abstract Base Class (ABC).

    """
    @abstractmethod
    def __init__(self):
        """
        MeasurementEngine constructor

        """
        pass


    @abstractmethod
    def psd():
        """
        Computes and returns a Power Spectral Density (PSD) estimate

        Args:
            iq (np.ndarray): Numpy array of complex IQ data to compute the PSD on
            fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
            nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
            window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
            center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
            scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
            multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

        Returns:
            A tuple with:
                f_welch (np.ndarray): The frequency vector associated with the PSD results.  Centered at
                                    center_frequency_hz.
                s_welch (np.ndarray): The PSD results.

        """
        pass


    @abstractmethod
    def spectrogram():
        """
        Computes and returns a spectrogram (aka waterfall) estimate

        Args:
            iq (np.ndarray): Numpy array of complex IQ data to compute the spectrogram on
            fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
            nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
            nperseg (int, optional): Number of samples per segment.  Defaults to 1024.
            noverlap (int, optional): Number of samples of overlap between segments.  Defaults to 128.
            window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
            center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
            scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
            multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

        Returns:
            A tuple with:
                f_combined (np.ndarray): The frequency vector associated with the spectrogram results.  Centered at
                                         center_frequency_hz.
                t_combined (np.ndarray): The time vector associated with the spectrogram results.  Starts at 0.0.
                sxx_combined (np.ndarray): The spectrogram PSD results.

        """
        pass
