import numpy as np
from dsp_utilities.measurement_engine.measurement_engine_abc import MeasurementEngine_Plugin_ABC
from dsp_utilities.measurements import psd, spectrogram

class MeasurementEngine(MeasurementEngine_Plugin_ABC):
    """A measurement engine implementation for performing basic spectrum and carrier measurements

    This class implements the measurement engine described by the MeasurementEngine_Plugin_ABC Abstract Base Class
    (ABC).

    """

    def __init__(self):
        """
        MeasurementEngine constructor

        """
        self.description = 'The stock dsp_utilities measurement engine'


    def psd(self, iq:np.ndarray, fs_hz:float = 1.0, nfft:int = 1024, window:str = 'hann',
            center_frequency_hz:float = 0.0, scale:str = 'log', multi_threaded:bool = False):
        """
        Computes and returns a Power Spectral Density (PSD) estimate

        Args:
            iq (np.ndarray): Numpy array of complex IQ data to compute the PSD on
            fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
            nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
            window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
            center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
            scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
            multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

        Returns:
            A tuple with:
                f_welch (np.ndarray): The frequency vector associated with the PSD results.  Centered at
                                    center_frequency_hz.
                s_welch (np.ndarray): The PSD results.

        """
        f_welch, s_welch = psd(iq=iq, fs_hz=fs_hz, nfft=nfft, window=window, center_frequency_hz=center_frequency_hz,
                               scale=scale, multi_threaded=multi_threaded)
        return f_welch, s_welch


    def spectrogram(self, iq:np.ndarray, fs_hz: float = 1.0, nfft: int = 1024, nperseg: int = 1024, noverlap: int = 128,
                    window: str = 'hann', center_frequency_hz: float = 0.0, scale: str = 'log',
                    multi_threaded: bool = False):
        """
        Computes and returns a spectrogram (aka waterfall) estimate

        Args:
            iq (np.ndarray): Numpy array of complex IQ data to compute the spectrogram on
            fs_hz (float, optional): Sample rate in hz.  Defaults to 1.0.
            nfft (int, optional): Number of FFT bins used in the calculation.  Defaults to 1024.
            nperseg (int, optional): Number of samples per segment.  Defaults to 1024.
            noverlap (int, optional): Number of samples of overlap between segments.  Defaults to 128.
            window (str, optional): Window used in PSD calculation.  Defaults to 'hann'.
            center_frequency_hz (float, optional): Center frequency in Hz of IQ data.  Defaults to 0.0.
            scale (str, optional): Scale of returned PSD data.  Either 'log' 'linear'.  Defaults to 'log'
            multi_threaded (bool, optional): Run parallelized implementation.  Defaults to false.

        Returns:
            A tuple with:
                f_combined (np.ndarray): The frequency vector associated with the spectrogram results.  Centered at
                                         center_frequency_hz.
                t_combined (np.ndarray): The time vector associated with the spectrogram results.  Starts at 0.0.
                sxx_combined (np.ndarray): The spectrogram PSD results.

        """
        f_combined, t_combined, sxx_combined = \
            spectrogram(iq=iq, fs_hz=fs_hz, nfft=nfft, nperseg=nperseg, noverlap=noverlap, window=window,
                        center_frequency_hz=center_frequency_hz, scale=scale, multi_threaded=multi_threaded)

        return f_combined, t_combined, sxx_combined