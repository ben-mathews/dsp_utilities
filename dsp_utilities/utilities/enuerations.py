from enum import Enum

class ModulationType(Enum):
    MOD_UNKNOWN = 0
    MOD_BPSK = 1
    MOD_QPSK = 2
    MOD_OQPSK = 3
    MOD_8PSK = 4
    MOD_16APSK = 5
    MOD_32APSK = 6