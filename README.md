# dsp_utilities

Misc DSP-related code that I find useful to reuse

## Contents

* file_handlers: Contains the iq_file class and various iq_file_plugin classes to provide functionality for reading data
  from a variety of IQ file types, including X-Midas BLUE format, Matlab .mat format, and raw formats.  Python
  Abstract Base Class (ABC) plugin architecture facilitates easily adding support for other file types.  See 
  iq_file_plugin_abc.py base class for details.
* measurement_engine: An abstract base class defintion and implementation of a measurement engine to perform basic
  signal measurements.  Uses functility in the measurements module.
* measurements: Basic measurement functionality.
* plotting: Routines for generating useful plots, including a PSD plot and scatterplot. 
