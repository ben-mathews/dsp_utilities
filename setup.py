#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='dsp_utilities',
      version='0.0.1',
      description='Python 3 utility functions for DSP processing',
      author="Ben Mathews",
      author_email="ben@nothing.com",
      url="https://gitlab.com/ben-mathews/dsp_utilities",
      install_requires=[
          "scipy>=1.2.1",
          "numpy>=1.16.3",
          "matplotlib>=3.0.3",
      ],
      packages=['dsp_utilities', 'dsp_utilities.file_handlers', 'dsp_utilities.plotting']
     )

